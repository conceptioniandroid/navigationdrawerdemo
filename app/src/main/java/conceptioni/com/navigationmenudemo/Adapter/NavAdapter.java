package conceptioni.com.navigationmenudemo.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import conceptioni.com.navigationmenudemo.R;

public class NavAdapter extends BaseExpandableListAdapter {
    Context context;
    View view;
    private List<String> _listDataHeader; // header titles
    private List<Integer> _listDataImage; // Images
    // child data in format of header title, child title
    private HashMap<String, List<String>> _listDataChild;

    public NavAdapter(@NonNull Context context, List<String> _listDataHeader, HashMap<String, List<String>> _listDataChild, List<Integer> integerList ) {
        this.context=context;
        this._listDataHeader=_listDataHeader;
        this._listDataChild=_listDataChild;
        this._listDataImage=integerList;
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {

        if (_listDataChild.get(this._listDataHeader.get(groupPosition)) != null) {
            return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                    .size();
        }
        return 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.rowitem_nav, null);
        }
        RelativeLayout linearLayout= (RelativeLayout) convertView.findViewById(R.id.llnavMain);
        final ImageView imageView= (ImageView) convertView.findViewById(R.id.ivdown);
        final ImageView ivNav= (ImageView) convertView.findViewById(R.id.ivNav);
        TextView lblListHeader = (TextView) convertView.findViewById(R.id.tvrNav);
//        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/POPPINS-REGULAR_0.TTF");
//        lblListHeader.setTypeface(font);

        ivNav.setImageResource(_listDataImage.get(groupPosition));
        lblListHeader.setText(headerTitle);

        if (_listDataChild.get(this._listDataHeader.get(groupPosition)) == null){
            imageView.setVisibility(View.GONE);
        }else {
            imageView.setVisibility(View.VISIBLE);
        }
        if (isExpanded) {
            imageView.setImageResource(R.drawable.arrow_up);
        } else {
            imageView.setImageResource(R.drawable.arrow_down);
        }
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.rowitem_nav_inner, null);
        }

        TextView txtListChild = (TextView) convertView.findViewById(R.id.tvrNavChild);
//        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/POPPINS-REGULAR_0.TTF");
//        txtListChild.setTypeface(font);
        ImageView imageView = convertView.findViewById(R.id.ivNavinner);
        txtListChild.setText(childText);
        if (childPosition==0){
            imageView.setImageResource(R.drawable.call);
        }else{
            imageView.setImageResource(R.drawable.call);
        }

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
