package conceptioni.com.navigationmenudemo;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Fade;
import android.transition.Visibility;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ExpandableListView;
import android.widget.TextView;
import com.easyandroidanimations.library.FadeInAnimation;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import conceptioni.com.navigationmenudemo.Adapter.NavAdapter;
import conceptioni.com.navigationmenudemo.fragment.HomeFragment;

public class MainActivity extends AppCompatActivity {
    DrawerLayout drawerLayout;
    Toolbar toolbar;
    ExpandableListView lvExp;
    NavigationView nav_view;
    List<String> listDataHeader;
    List<Integer> listDataImage;
    HashMap<String, List<String>> listDataChild;
    NavAdapter listAdapter;
    TextView toolbar_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        clicks();
    }

    private void clicks() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                draweropen();
            }
        });

        lvExp.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @SuppressLint("NewApi")
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int groupPosition, long id) {
                if (groupPosition == 0) {
                    toolbar_title.setText(R.string.home);
                    HomeFragment fragment = new HomeFragment();
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction ft = fragmentManager.beginTransaction();
                    ft.replace(R.id.content, fragment);
                    drawerLayout.closeDrawers();
                    ft.commit();
                } else if (groupPosition == 1) {
                    toolbar_title.setText(R.string.Gallery);
                    drawerLayout.closeDrawers();
                } else if (groupPosition == 2) {
                  new FadeInAnimation(view).animate();
                } else if (groupPosition == 3) {
                    toolbar_title.setText(R.string.Settings);
                    drawerLayout.closeDrawers();
                } else if (groupPosition == 4) {
                    toolbar_title.setText(R.string.Share);
                    drawerLayout.closeDrawers();
                }//
                return false;
            }
        });
    }

    private void init() {
        toolbar = findViewById(R.id.toolbar);
        toolbar_title = findViewById(R.id.toolbar_title);
        nav_view = findViewById(R.id.nav_view);
        lvExp = findViewById(R.id.lvExp);
        drawerLayout = findViewById(R.id.drawer_layout);
        prepareListData();

        listAdapter = new NavAdapter(this, listDataHeader, listDataChild, listDataImage);
        lvExp.setAdapter(listAdapter);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(R.id.content, new HomeFragment());
        ft.commit();


    }

    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataImage = new ArrayList<Integer>();
        listDataChild = new HashMap<String, List<String>>();

        // Adding child data
        listDataHeader.add(getString(R.string.home));
        listDataHeader.add(getString(R.string.Gallery));
        listDataHeader.add(getString(R.string.contact));
        listDataHeader.add(getString(R.string.Settings));
        listDataHeader.add(getString(R.string.Share));

        listDataImage.add(R.drawable.home_black);
        listDataImage.add(R.drawable.photo_library);
        listDataImage.add(R.drawable.call);
        listDataImage.add(R.drawable.ic_settings_black_24dp);
        listDataImage.add(R.drawable.ic_share_black_24dp);

        List<String> nowShowing = new ArrayList<String>();
        nowShowing.add(getString(R.string.mobileno));
        nowShowing.add(getString(R.string.whatsno));


        listDataChild.put(listDataHeader.get(2), nowShowing);
    }

    public void draweropen() {
        drawerLayout.openDrawer(GravityCompat.START);
    }
}
